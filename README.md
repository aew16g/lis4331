> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4331 - Advanced Mobile Application Development

## Alexander Wilson

### LIS4331 Requirements:

*Course Work Links:* 

##### 1. [A1 README.md](A1/README.md "My A1 README.md file")
- Install JDK
- Install Android Studio and create My First App
- Provide screenshots of installations
- Create Bitbucket repo
- Complete Bitbucket tutorials (bitbucketstationlocation and myteamquotes)
- provide git command descriptions

##### 2. [A2 README.md](A2/README.md "My A2 README.md file")
- Create Tip Calculator App
- Provides screenshots of completed app
- Skillsets 1-3

##### 3. [A3 README.md](A3/README.md "My A3 README.md file")
- Create Currency Converter
- Create Splash Screen
- Create Toast pop-ip
- Include skillsets 4-6

##### 4. [A4 README.md](A4/README.md "My A4 README.md file")
- Creation of Mortgage Calculator App
- Include Skilletsets 10-12

##### 5. [A5 README.md](A5/README.md "My A5 README.md file")
- Create RSS APP
- Include Skillsets 13-15

##### 6. [P1 README.md](P1/README.md "My P1 README.md file")
- Create My Music App
- Create Pause/Play function
- create splash page
- Include Skillsets 7-9
 

##### 7. [P2 README.md](P2/README.md "My P2 README.md file")
- Creation of My User's App
- Creation of Splash Screen