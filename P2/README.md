> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4331 - Advanced Mobile Application Development
## Alexander Wilson

### Project 2 Requirements:

Three Parts:

1. Create Users app that Adds, Updates, Deletes and Views user accounts. 
2. Create Splash Screen

#### README.md file should include the following items:

* Screenshot running App
* Screenshot running Splash Screen

#### Assignment Screenshots:



| Screenshot of Splash Screen                        | Screenshot of Add                                  |
|----------------------------------------------------|----------------------------------------------------|     
|![java Hello](img/splash.png)                       | ![MyFirst App](img/add.png)                        |

| Screenshot of Added                                |    Screenshot of View (1)                          |
|----------------------------------------------------|----------------------------------------------------|
|![First User Interface](img/added.png)              |  ![Second User Interface](img/firstview.png)       |

| Screenshot of Update                               | Screenshot of Updated                              |
|----------------------------------------------------|----------------------------------------------------|     
|![java Hello](img/update.png)                       | ![MyFirst App](img/updated.png)                    |

| Screenshot of View (2)                             |    Screenshot of Delete                           |
|----------------------------------------------------|----------------------------------------------------|
|![First User Interface](img/view.png)               |  ![Second User Interface](img/delete.png)         |

| Screenshot of Deleted                              |    Screenshot of Deleted Database                  |
|----------------------------------------------------|----------------------------------------------------|
|![First User Interface](img/deleted.png)            |  ![Second User Interface](img/cleared.png)         |