# LIS4331 - Advanced Mobile Application Development
## Alexander Wilson

### Assignment 5 Requirements:

Three Parts:

1. Creation of RSS App
2. Skillsets 13-15 development


#### README.md file should include the following items:

* Screenshot running App
* Screenshot running App redirection
* Screenshot running Skillsets 13-15


#### Assignment Screenshots:




| Screenshot of Homepage                             | Screenshot of News                                 |
|----------------------------------------------------|----------------------------------------------------|     
|![java Hello](img/home.png)                         | ![MyFirst App](img/second.png)                     |

| Screenshot of Read More:                           |    Skill set 13:                                   |
|----------------------------------------------------|----------------------------------------------------|
|![First User Interface](img/third.png)              |  ![Second User Interface](img/ss13.png)            |

| Screenshot of Skillset 14:                         |    Screenshot of Skillset 15:                      |
|----------------------------------------------------|----------------------------------------------------|
|![First User Interface](img/ss14.png)               |  ![Second User Interface](img/ss15.png)            |
