# LIS4331 - Advanced Mobile Application Development
## Alexander Wilson

### Assignment 2 Requirements:

Three Parts:

1. Creation of Tip Calculator App
2. Skillsets 1-3 development


#### README.md file should include the following items:

* Screenshot running Unpopulated App
* Screenshot running Populated App
* Screenshot running Skillsets 1-3


#### Assignment Screenshots:




| Screenshot of unpopulated App                      | Screenshot of Populated App                        |
|----------------------------------------------------|----------------------------------------------------|     
|![java Hello](img/appone.png)                         | ![MyFirst App](img/apptwo.png)                   |

| Screenshot of Skillset 1:                          |    Screenshot of Skillset 2:                       |
|----------------------------------------------------|----------------------------------------------------|
|![First User Interface](img/ss1.png)                |  ![Second User Interface](img/ss2.png)             |

|                                         Screenshot of Skillset 3:                                       |
|---------------------------------------------------------------------------------------------------------|
|![third skillset](img/ss3.png)                                                                           |

#### Tutorial Links:

Bitbucket Tutorial:
[A1 BitbucketStationLocations Tutorial](https://bitbucket.org/aew16g/bitbucketstationlocation/src/master/ "Bitbucket Station Locations")
