# LIS4331 - Advanced Mobile Application Development
## Alexander Wilson

### Assignment 3 Requirements:

Three Parts:

1. Creation of Currency Converter App
2. Skillsets 4-6 development


#### README.md file should include the following items:

* Screenshot running Unpopulated App
* Screenshot running Populated App
* Screenshot running Skillsets 4-6


#### Assignment Screenshots:



| Screenshot of unpopulated App                      | Screenshot of Populated App                        |
|----------------------------------------------------|----------------------------------------------------|     
|![java Hello](img/unpopulated.png)                  | ![MyFirst App](img/populated.png)                  |

| Screenshot of Toast:                               |    Screenshot of Splash:                           |
|----------------------------------------------------|----------------------------------------------------|
|![First User Interface](img/toast.png)              |  ![Second User Interface](img/splash.png)          |

| Screenshot of SS4:                                 | Screenshot of SS5:                                 |
|----------------------------------------------------|----------------------------------------------------|     
|![java Hello](img/SS4.png)                          | ![MyFirst App](img/ss5pt1.png)                     |

| Screenshot of SS5:                                 | Screenshot of SS5:                                 |
|----------------------------------------------------|----------------------------------------------------|
|![First User Interface](img/ss5pt2.png)             |  ![Second User Interface](img/ss5pt3.png)          |

| Screenshot of SS5:                                 | Screenshot of SS6:                                 |
|----------------------------------------------------|----------------------------------------------------|
|![First User Interface](img/ss5pt4.png)             |  ![Second User Interface](img/ss6pt1.png)          |

| Screenshot of SS6:                                 | Screenshot of SS6:                                 |
|----------------------------------------------------|----------------------------------------------------|     
|![java Hello](img/ss6pt2.png)                       | ![MyFirst App](img/ss6pt3.png)                     |

| Screenshot of SS6:                                 | Screenshot of SS6:                                 |
|----------------------------------------------------|----------------------------------------------------|     
|![java Hello](img/ss6pt4.png)                       | ![MyFirst App](img/ss6pt5.png)                     |

| Screenshot of SS6:                                                                                      |
|---------------------------------------------------------------------------------------------------------|
| ![SS6](img/ss6pt6.png)                                                                                  |

#### Tutorial Links:

Bitbucket Tutorial:
[A1 BitbucketStationLocations Tutorial](https://bitbucket.org/aew16g/bitbucketstationlocation/src/master/ "Bitbucket Station Locations")
