> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4331 - Advanced Mobile Application Development
## Alexander Wilson

### Project 1 Requirements:

Three Parts:

1. Create music app that plays music and pauses
2. Create Splash Screen
3. Include Skillsets 7,8 and 9

#### README.md file should include the following items:

* Screenshot running App
* Screenshot running Splash Screen
* Screenshot running pause app
* Screenshot playing app
* Screenshots of Skillsets

#### Assignment Screenshots:




| Screenshot of App                                  | Screenshot of Splash Screen                        |
|----------------------------------------------------|----------------------------------------------------|     
|![java Hello](img/artists.png)                      | ![MyFirst App](img/splash.png)                     |

| Screenshot of Playing Screen                       |    Screenshot of Pause Screen                      |
|----------------------------------------------------|----------------------------------------------------|
|![First User Interface](img/play.png)               |  ![Second User Interface](img/pause.png)           |

| Screenshot of SS7                                  | Screenshot of SS8                                  |
|----------------------------------------------------|----------------------------------------------------|     
|![java Hello](img/ss7.png)                          | ![MyFirst App](img/ss8pt1.png)                     |

| Screenshot of SS8                                  |    Screenshot of SS9                               |
|----------------------------------------------------|----------------------------------------------------|
|![First User Interface](img/ss8pt2.png)             |  ![Second User Interface](img/ss9pt1.png)          |

| Screenshot of SS9                                  |Screenshot of SS9                                   |
|----------------------------------------------------|----------------------------------------------------|     
|![java Hello](img/ss9pt2.png)                       | ![MyFirst App](img/ss9pt3.png)                     |