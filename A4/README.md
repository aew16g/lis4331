# LIS4331 - Advanced Mobile Application Development
## Alexander Wilson

### Assignment 4 Requirements:

Three Parts:

1. Creation of Mortgage Calculator App
2. Skillsets 10-12 development


#### README.md file should include the following items:

* Screenshot running Unpopulated App
* Screenshot running Populated App
* Screenshot running Skillsets 10-12


#### Assignment Screenshots:




| Screenshot of Homepage                             | Screenshot of Splash                               |
|----------------------------------------------------|----------------------------------------------------|     
|![java Hello](img/home.png)                       | ![MyFirst App](img/splash.png)                       |

| Screenshot of Results:                             |    Screenshot of Error:                            |
|----------------------------------------------------|----------------------------------------------------|
|![First User Interface](img/results.png)            |  ![Second User Interface](img/error.png)           |

| Screenshot of Skillset 10:                         |    Screenshot of Skillset 11:                      |
|----------------------------------------------------|----------------------------------------------------|
|![First User Interface](img/ss10.png)               |  ![Second User Interface](img/ss11.png)            |

|                                         Screenshot of Skillset 12:                                      |
|---------------------------------------------------------------------------------------------------------|
|![third skillset](img/ss12.png)                                                                          |
